Winnowing
=========

This is an implementation of Winnowing algorithm with rolling hash

External Lib needed :

1. commons-codec-1.5.jar
2. commons-logging-1.1.1.jar
3. dom4j01.6.1.jar
4. fontbox-1.8.5.jar
5. jempbox-1.8.5.jar
6. pdfbox-1.8.5.jar
7. poi-3.10-FINAL.jar
8. poi-ooxml-3.10-FINAL.jar
9. poi-ooxml-schemas-3.10-FINAL.jar
10. poi-scratchpad-3.10-FINAL.jar
11. stax-api-1.0.1.jar
12. xml-apis-1.0.b2.jar
13. xmlbeans-2.3.0.jar