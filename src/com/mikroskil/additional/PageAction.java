/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.additional;

import javax.swing.JFrame;

/**
 *
 * @author Andy Wijaya
 */
public class PageAction {
    private JFrame owner;
       
    public PageAction(JFrame owner){
        this.owner = owner;
    }

    public JFrame getOwner() {
        return owner;
    }
}