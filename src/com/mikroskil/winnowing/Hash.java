/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;

/**
 *
 * @author Andy Wijaya
 */
public class Hash {
    private long hash;
    private int marked;
    
    public Hash(){
        marked = 0;
    }
    
    public Hash(long hash){
        this.hash = hash;
    }

    public void setHash(long hash) {
        this.hash = hash;
    }
    
    public long getHash(){
        return this.hash;
    }
    
    public void mark(){
        marked = 1;
    }
    
    public int getMarked(){
        return marked;
    }

    @Override
    public boolean equals(Object o) {
        boolean retVal = false;
        
        if(o instanceof Hash){
            Hash ptr = (Hash)o;
            retVal = ptr.getHash() == this.getHash();
        }
        return retVal;
    }
    
    
}