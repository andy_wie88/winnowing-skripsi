/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;

import java.util.*;
/**
 *
 * @author Andy Wijaya
 */
public class Reports {
    
    private List<Record> result;
    
    public Reports(){
        result = new ArrayList<Record>();
    }
    
    public void addRecord(Record r){
        result.add(r);
    }
    
    public int getNumRecords(){
        return result.size();
    }
    
    public Record getRecordAt(int index){
        return result.get(index);
    }
}
