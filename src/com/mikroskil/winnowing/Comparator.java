/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;
import java.util.*;

/**
 *
 * @author Andy Wijaya
 */
public class Comparator {
    
    FingerPrint originFootPrint,vsFootPrint;
    double percentage;
    
    public Comparator(FingerPrint doc1,FingerPrint doc2){
        originFootPrint = doc1;
        vsFootPrint = doc2;
    }
    
    /** Jaccard's Coefficient **/
    public double copyPercentage(){
        int copy = intersection();
        int union = union(originFootPrint.getFingerPrint(),vsFootPrint.getFingerPrint());
        double result = 100 * (copy/(double)union);
        System.out.println("J = "+copy+" / "+union+" x 100% = "+result);
        return result;
    }
    
    public int intersection(){
        int copy = 0;
        long originHash = 0;
        boolean notFound = true;
        while(originFootPrint.hasMoreHash()){
            originHash = originFootPrint.getNextHash();
            while(vsFootPrint.hasMoreHash() && notFound){
                if(originHash == vsFootPrint.getNextHash()){
                    originFootPrint.mark();
                    ++copy;
                    notFound = false;
                }
            }
            vsFootPrint.reset();
            notFound = true;
        }
        return copy;
    }
    
    public int union(List<Hash> x, List<Hash> y){
        ArrayList<Hash> union = new ArrayList<>(x);
        for(Hash t:y){
            if(!union.contains(t))
                union.add(t);
        }
        return union.size();
    }
}