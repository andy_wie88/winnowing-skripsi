/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;


import java.util.*;
import java.io.*;

/**
 *
 * @author Andy Wijaya
 */
public class Parameter {
    
    private int basis;
    private int k_gram;
    private int window_size;
    int plagiatPercentage;
    private List<File> compareDocument;
    private List<File> documentBD;
    
    
    public Parameter(){
        basis = 3;
        k_gram = 9;
        window_size = 5;
        plagiatPercentage = 20;
        compareDocument = new ArrayList<File>();
        documentBD = new ArrayList<File>();
    }
    
    public Parameter(int k_gram, int window_size){
        this.k_gram = k_gram;
        this.window_size = window_size;
        compareDocument = new ArrayList<File>();
        documentBD = new ArrayList<File>();
    }
    
    public Parameter(int k_gram, int window_size, int basis){
        this.basis = basis;
        this.k_gram = k_gram;
        this.window_size = window_size;
        compareDocument = new ArrayList<File>();
        documentBD = new ArrayList<File>();
    }
        
    
    public int getMaxPercentage(){
        return plagiatPercentage;
    }
    
    public int get_k(){
        return k_gram;
    }
    
    public void set_k(int k_gram){
        this.k_gram = k_gram;
    }
    
    public int get_w(){
        return window_size;
    }
    
    public void set_w(int window_size){
        this.window_size = window_size;
    }
    
    public File getDocDisco(int index){
        return compareDocument.get(index);
    }
    
    public void addDocument(File document){
        this.compareDocument.add(document);
    }
    
    public void setDocList(List<File> documents){
        this.compareDocument =documents;
    }
    
    public int getNumDocsDisco(){
        return compareDocument.size();
    }
    
    public File getIDocBD(int index){
        return documentBD.get(index);
    }
    
    public void addIDBD(File file){
        documentBD.add(file);
    }
    
    public void setIDBDList(List<File> files){
        this.documentBD = files;
    }
    
    public int getNumDocBD(){
        return documentBD.size();
    }

    public int getBasis() {
        return basis;
    }

    public void setBasis(int basis) {
        this.basis = basis;
    }
    
}
