/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;

import java.util.*;

/**
 *
 * @author Andy Wijaya
 */
public class Window {
    
    private int windowSize;
    
    private int limit;
    
    private int minHash;
    
    private List<Hash> window;
    
    private boolean newMin;
    
    public Window(){
        this.windowSize = 5;
        this.window = new ArrayList<Hash>(windowSize);
    }
    
    public Window(int windowSize){
        this.windowSize = windowSize;
        this.window = new ArrayList<Hash>();
        for (int i=0;i<windowSize;i++){
            window.add(new Hash());
        }
    }
    
    public void addHash(Hash hash){
        ++limit;
        limit %= windowSize;
        window.set(limit, new Hash(hash.getHash()));
        calculateNewMin();
    }
    
    private void calculateNewMin(){
        if(limit == minHash){
            newMin = true;
            minHash = 0;
            for(int i =1;i<windowSize;++i){
                if(window.get(i).getHash()<window.get(minHash).getHash()){
                    minHash = i;
                }
            }
        }
        else{
            if(window.get(limit).getHash() <= window.get(minHash).getHash()){
                newMin = true;
                minHash = limit;
            }
            else{
                newMin = false;
            }
        }
    }
    
    public boolean isNewMin(){
        return this.newMin;
    }
    
    public long getMin(){
        return window.get(minHash).getHash();
    }
}