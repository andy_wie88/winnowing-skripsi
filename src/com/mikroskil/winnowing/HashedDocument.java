/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;

import java.util.*;
/**
 *
 * @author Andy Wijaya
 */
public class HashedDocument {
    private List<Hash> hashCode;
    private int position;
    private List<String> document;
    
    public HashedDocument(){
        
    }
    
    public HashedDocument(List<String> document, Parameter parameter){
        this.document = document;
        hasHearDocument(parameter.get_k(),parameter.getBasis());
    }
    
    public boolean isProcess(){
        return position < hashCode.size();
    }
    
    public Hash nextHash(){
        Hash temp = hashCode.get(position);
        position++;
        return temp;
    }
    
    public void hasHearDocument(int k,int b){
        hashCode = new ArrayList<Hash>();
        StringBuffer doc = new StringBuffer();
        for(int i=0;i<this.document.size();i++){
            doc.append(this.document.get(i));
        }
        String docJuntado = doc.toString();
        for(int i=0;i<docJuntado.length()-(k-1);i++){
            String palabra = docJuntado.substring(i,i+k);
            System.out.println("H"+(i+1)+" = "+palabra);
            long temp = hashing(palabra,b,i);
            hashCode.add(new Hash(temp));
            System.out.println();
            System.out.println("H"+(i+1)+" = "+temp+"\n");
        }
    }
    
    /** Rolling Hash**/
    public long hashing(String key,int b,int index){
        long h=0;
        int j=1;
        System.out.print("H"+(index+1)+" = ");
        for(int i=0;i<key.length();i++){
            if(i==0)
                System.out.print((int)key.charAt(i)+"*"+b+"^("+(key.length()-i)+"-1)");
            else
                System.out.print("+"+(int)key.charAt(i)+"*"+"^("+b+"-"+(key.length()-i)+"-1)");
        }
        System.out.println();
        System.out.print("H"+(index+1)+" = ");
        for(int i=0;i<key.length();i++){
            long temp = (int)key.charAt(i)*(long)Math.pow(b,key.length()-j);
            h +=temp;
            if(i==0)
                System.out.print(temp);
            else
                System.out.print("+"+temp);
            j++;
        }
        return h;
    }
    
    public int getPositionOf(Hash hash){
        return hashCode.indexOf(hash);
    }
    
    public void showInWindow(int windowSize){
        int i=0;
        while((i+windowSize)<=hashCode.size()){
            System.out.print("{");
            for(int j=i;j<i+windowSize;j++){
                if(j==i)
                    System.out.print(hashCode.get(j).getHash());
                else
                    System.out.print(" "+hashCode.get(j).getHash());
            }
            System.out.println("}");
            i++;
        }
    }
}