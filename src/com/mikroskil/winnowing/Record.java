/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;

/**
 *
 * @author Andy Wijaya
 */
public class Record {
    
    private String originalDocument;
    private String compareDocument;
    private double percentage;

    public Record(String originalDocument, String compareDocument, double percentage) {
        this.originalDocument = originalDocument;
        this.compareDocument = compareDocument;
        this.percentage = percentage;
    }

    public String getOriginalDocument() {
        return originalDocument;
    }

    public void setOriginalDocument(String originalDocument) {
        this.originalDocument = originalDocument;
    }

    public String getCompareDocument() {
        return compareDocument;
    }

    public void setCompareDocument(String compareDocument) {
        this.compareDocument = compareDocument;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }
    
}
