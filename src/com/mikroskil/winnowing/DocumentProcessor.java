/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

public class DocumentProcessor {
    
    private File originalDocument;
    private List<String> document;
    private String number;
    
    private String getNumber(){
        return number;
    }
    
    public DocumentProcessor(File originalDocument){
        this.originalDocument = originalDocument;
        processDocument(false);
    }
    
    public List<String> processDocument(boolean isSpace){
        List<String> output = new ArrayList<String>();
        String result="";
        if(getExtension(originalDocument).equalsIgnoreCase("doc")){
            try {
                POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(originalDocument));
                HWPFDocument document = new HWPFDocument(fs);
                WordExtractor we = new WordExtractor(document);
                String[] paragraphs = we.getParagraphText();
                we.close();
                for(int i=0;i<paragraphs.length;i++){
                    result+=paragraphs[i].toString();
                }
            } catch (IOException ex) {
                Logger.getLogger(DocumentProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(getExtension(originalDocument).equalsIgnoreCase("docx")){
            try{
                XWPFDocument document = new XWPFDocument(new FileInputStream(originalDocument));
                XWPFWordExtractor we = new XWPFWordExtractor(document);
                result = we.getText();
                we.close();
            }catch(IOException ex){
                Logger.getLogger(DocumentProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if (getExtension(originalDocument).equalsIgnoreCase("pdf")) {
            try {
                PDFTextStripper pdfs = new PDFTextStripper();
                PDDocument pdoc = new PDDocument();
                pdoc = PDDocument.load(originalDocument);
                result = pdfs.getText(pdoc);
                pdoc.close();
            } catch (IOException ex) {
                Logger.getLogger(DocumentProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        StringTokenizer st = new StringTokenizer(result, ".");
        while(st.hasMoreElements()){
            output.add(processLine(st.nextToken(), isSpace));
        }
        
        for(int i=0;i<output.size();i++){
            if(output.get(i).equalsIgnoreCase(""))
                output.remove(i);
        }
        document = output;
        return output;
    }
    
    public List<String> getDocument(){
        return document;
    }
    
    private String getExtension(File f){
        String fileName = f.getName();
        int i = fileName.lastIndexOf(".");
        String ext="";
        if(i>=0)
            ext = fileName.substring(i+1);
        return ext;
    }
    
    private String processLine(String line,boolean isSpace){
        line = line.toLowerCase();
        line = line.replaceAll("[á]", "a");
        line = line.replace("\u00e9", "e");
        line = line.replace('í', 'i');
        line = line.replace('ó', 'o');
        line = line.replace('ú', 'u');
        line = line.replace('ñ', 'n');
        line = line.replaceAll("[^ a-z0-9]", "");
        String lineaProcesada = new String();
        if (!isSpace)
            line = line.replaceAll("[^a-z0-9]", "");
        else
            line = line.replaceAll("[^ a-z0-9]", "");
        lineaProcesada = line;
        return lineaProcesada;
    }
    
    public int size(){
        int size = 0;
        for(int i=0;i<document.size();++i){
            size+=document.get(i).length();
        }
        return size;
    }
}
