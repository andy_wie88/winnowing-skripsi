/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;

/**
 *
 * @author Andy Wijaya
 */
public class Winnow {
    
    private FingerPrint fingerPrint;
    private Window window;
    private HashedDocument document;
    private int windowSize;
    
    public FingerPrint getFingerPrint(){
        return fingerPrint;
    }
    
    public Winnow(){
        
    }
    
    public Winnow(int windowSize,HashedDocument document){
        this.fingerPrint = new FingerPrint();
        this.window = new Window(windowSize);
        this.document = document;
        this.windowSize = windowSize;
        createFingerPrint();
    }
    
    private void createFingerPrint(){
        for(int i=0;i<windowSize-1;i++)
            window.addHash(document.nextHash());
        while(document.isProcess()){
            window.addHash(document.nextHash());
            if(window.isNewMin())
                fingerPrint.add(window.getMin(),document.getPositionOf(new Hash(window.getMin())));
        }
        
        document.showInWindow(windowSize);
        System.out.println("FingerPrint : ");
        fingerPrint.showList();
        System.out.println();
    }
}
