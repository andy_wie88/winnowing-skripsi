/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;

/**
 *
 * @author Andy Wijaya
 */
public class Winnowing {
    private Reports report;
    
    public Winnowing(Parameter parameter){
        report = new Reports();
        
        for(int i=0;i<parameter.getNumDocsDisco();i++){
            DocumentProcessor original = new DocumentProcessor(parameter.getDocDisco(i));
            
            System.out.println(parameter.getDocDisco(i).getName());
            
            HashedDocument hashDoc = new HashedDocument(original.getDocument(),parameter);
            
            
            Winnow winDoc = new Winnow(parameter.get_w(),hashDoc);
            
            for(int j=0;j<parameter.getNumDocBD();j++){
                
                System.out.println(parameter.getIDocBD(j).getName());
                
                DocumentProcessor docBD = new DocumentProcessor(parameter.getIDocBD(j));
                
                HashedDocument hashBD = new HashedDocument(docBD.getDocument(),parameter);
                
                Winnow winBD = new Winnow(parameter.get_w(),hashBD);
                
                Comparator comparator = new Comparator(winDoc.getFingerPrint(), winBD.getFingerPrint());
                
                winDoc.getFingerPrint().reset();
                report.addRecord(new Record(parameter.getDocDisco(i).getName(),parameter.getIDocBD(j).getName(),comparator.copyPercentage()));
            }
        }
        
    }
    
    public Reports getReport(){
        return report;
    }
}
