/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing;

import java.util.*;

/**
 *
 * @author Andy Wijaya
 */
public class FingerPrint {
    
    private List<Hash> fingerPrint;
    private int position;
    private List<Integer> marked;
    
    public FingerPrint(){
        fingerPrint = new ArrayList<Hash>();
        marked = new ArrayList<Integer>();
        position = 0;
    }
    
    public void add(long hash,int pos){
        fingerPrint.add(new Hash(hash));
        marked.add(pos);
    }
    
    public long getNextHash(){
        if(hasMoreHash()){
            long temp = fingerPrint.get(position).getHash();
            position++;
            return temp;
        }
        else
            return 0;
    }
    
    public boolean hasMoreHash(){
        return position < fingerPrint.size();
    }
    
    public void reset(){
        position = 0;
    }
    
    public int size(){
        return fingerPrint.size();
    }
    
    public void mark(){
        fingerPrint.get(position-1).mark();
    }
    
    public int marked(){
        int marked = 0;
        for(int i=0;i<fingerPrint.size();i++){
            marked += fingerPrint.get(i).getMarked();
        }
        return marked;
    }
    
    public List<Hash> getFingerPrint(){
        return fingerPrint;
    }
    
    public void showList(){
        for(int i=0;i<fingerPrint.size();i++){
            if(i==0)
                System.out.print("["+fingerPrint.get(i).getHash()+","+marked.get(i)+"]");
            else
                System.out.print(",["+fingerPrint.get(i).getHash()+","+marked.get(i)+"]");
        }
        System.out.println();
    }
}
