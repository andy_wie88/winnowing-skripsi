/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mikroskil.winnowing_skripsi;

import com.mikroskil.additional.*;
import java.io.*;
import java.util.*;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.table.AbstractTableModel;
import com.mikroskil.winnowing.*;

/**
 *
 * @author Andy Wijaya
 */
public class MainAction extends PageAction {
    
    Reports reports;
    ArrayList<Record> report;
    public static final int ORIGINAL = 0;
    public static final int COMPARE = 1;
    
    private File originalDocument;
    private List<File> compareDocuments;
        
    public MainAction(JFrame owner) {
        super(owner);
        compareDocuments = new ArrayList<File>();
    }
    
    public void browseFile(int mode){
        JFileChooser jchooser = new JFileChooser();
        jchooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

            @Override
            public boolean accept(File f) {
                if(f.isDirectory())
                    return true;
                
                String fileName = f.getName();
                int i = fileName.lastIndexOf(".");
                String ext="";
                
                if(i>=0)
                    ext = fileName.substring(i+1);
                if(ext.equalsIgnoreCase("doc"))
                    return true;
                else if(ext.equalsIgnoreCase("docx"))
                    return true;
                else if(ext.equalsIgnoreCase("pdf"))
                    return true;
                
                return false;
            }

            @Override
            public String getDescription() {
                return "All Documents (*.doc/*docx/*.pdf)";
            }
        });
        jchooser.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {

            @Override
            public boolean accept(File f) {
                if(f.isDirectory())
                    return true;
                
                String fileName = f.getName();
                int i = fileName.lastIndexOf(".");
                String ext="";
                
                if(i>=0)
                    ext = fileName.substring(i+1);
                if(ext.equalsIgnoreCase("doc"))
                    return true;
                
                return false;
            }

            @Override
            public String getDescription() {
                return "Word 97-2003 Document (*.doc)";
            }
        });
        jchooser.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {

            @Override
            public boolean accept(File f) {
                if(f.isDirectory())
                    return true;
                
                String fileName = f.getName();
                int i = fileName.lastIndexOf(".");
                String ext="";
                
                if(i>=0)
                    ext = fileName.substring(i+1);
                if(ext.equalsIgnoreCase("docx"))
                    return true;
                
                return false;
            }

            @Override
            public String getDescription() {
                return "Word Document (*.docx)";
            }
        });
        jchooser.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {

            @Override
            public boolean accept(File f) {
                if(f.isDirectory())
                    return true;
                
                String fileName = f.getName();
                int i = fileName.lastIndexOf(".");
                String ext="";
                
                if(i>=0)
                    ext = fileName.substring(i+1);
                if(ext.equalsIgnoreCase("pdf"))
                    return true;
                
                return false;
            }

            @Override
            public String getDescription() {
                return "PDF Files (*.pdf)";
            }
        }); 
        if(mode==COMPARE)
            jchooser.setMultiSelectionEnabled(true);
        int returnVal = jchooser.showDialog(getOwner().getParent(),null); 
        if(returnVal == JFileChooser.APPROVE_OPTION){
            if(mode==ORIGINAL){
                File file = jchooser.getSelectedFile();
                originalDocument = file;
                ((Main)getOwner()).setOriginalDocumentText(file.getName());
            }
            else{
                for(File file : jchooser.getSelectedFiles()){
                    compareDocuments.add(file);
                }
                fillTable();
            }
        }
    }
    
    public void compare(){
        long start_time = System.currentTimeMillis();
        int gram = ((Main)getOwner()).getGram();
        int window = ((Main)getOwner()).getWindow();
        int basis = ((Main)getOwner()).getBasis();
        Parameter parameter = new Parameter(gram, window, basis);
        parameter.addDocument(originalDocument);
        parameter.setIDBDList(compareDocuments);
        Winnowing winnowing = new Winnowing(parameter);
        reports =  winnowing.getReport();
        long end_time = System.currentTimeMillis();
        ((Main)getOwner()).setTimeRender(getTimeRender(start_time, end_time));
        chooseReport();
        ((Main)getOwner()).setPlagiatPersentage(getPlagiatePersentage());
        ((Main)getOwner()).setJumlahPlagiat(report.size());
        fillResult();
    }
    
    public String getTimeRender(long start_time, long end_time){
        double time = end_time-start_time;
        time /=1000;
        int index=0;
        if(time>=60){
            while(true){
                double temp = time/60;
                if(temp>=1){
                    time = temp;
                    index++;
                }
                else
                    break;
            }
        }
        switch(index){
            case 0 :
                return String.format("%.2f "+((time==1)?"Second":"Seconds"),time);
            case 1 :
                return String.format("%2.f "+((time==1)?"Minute":"Minutes"),time);
            case 2 :
                return String.format("%2.f "+((time==1)?"Hour":"Hours"),time);
            default :
                return "Invalid";
        }
    }
    
    public double getPlagiatePersentage(){
        return ((double)report.size()/reports.getNumRecords())*100;
    }
    
    public void chooseReport(){
        ArrayList<Record> temp = new ArrayList<Record>();
        int length = reports.getNumRecords();
        for(int i=0;i<length;i++){
            if(reports.getRecordAt(i).getPercentage()>=50)
                temp.add(reports.getRecordAt(i));
        }
        report = temp;
    }
    
    public void fillResult(){
        ((Main)getOwner()).fillTableResult(new ResultListModel());
    }
    
    public void fillTable(){
        ((Main)getOwner()).fillTableCompareFile(new FileListModel());
    }
    
    private class FileListModel extends AbstractTableModel{
        private String columnName[] = {
            "No","File Name","Size"
        };
        
        @Override
        public int getRowCount() {
            return compareDocuments.size();
        }

        @Override
        public int getColumnCount() {
            return 3;
        }
        
        public String getColumnName(int column){
            return columnName[column];
        }
        
        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            File file = compareDocuments.get(rowIndex);
            switch(columnIndex){
                case 0:
                    return rowIndex+1;
                case 1:
                    return file.getName();
                case 2:
                    return convert(file.length());
                default:
                    return "Invalid index";
            }
        }
        
        private String convert(long length){
            String result ="";
            int index = 0;
            double temp = length;
            double temp2 = temp/1024;
            while(temp2>1){
                temp /= 1024;
                index++;
                temp2 /= 1024;
            }
            switch(index){
                case 1:
                    result = String.format("%.2f",temp)+" Kb";
                    break;
                case 2:
                    result = String.format("%.2f",temp)+" Mb";
                    break;
                case 3:
                    result = String.format("%.2f",temp)+" Gb";
                    break;
                default:
                    result = String.format("%.2f",temp)+" byte";
                    break;
            }
            return result;
        }
    }
    
    private class ResultListModel extends AbstractTableModel{
       
        private String[] columnName = {
            "Rank","File Name","Similarity[Persentage]"
        };
        @Override
        public int getRowCount() {
            return report.size();
        }

        @Override
        public int getColumnCount() {
           return 3;
        }
        
        public String getColumnName(int column){
            return columnName[column];
        }
        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Record temp = report.get(rowIndex);
            
            switch(columnIndex){
                case 0 :
                    return rowIndex+1;
                case 1 :
                    return temp.getCompareDocument();
                case 2:
                    return String.format("%.2f%%", temp.getPercentage());
                default:
                    return "Invalid Index";
            }
        }
        
    }
}